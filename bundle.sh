#!/bin/bash

BUNDLE_PATH=bundle

rm -rf $BUNDLE_PATH build node_modules
yarn install
yarn build

mkdir $BUNDLE_PATH
mv ./build ${BUNDLE_PATH}/dist/

yarn build:server
cp yarn.lock ${BUNDLE_PATH}
