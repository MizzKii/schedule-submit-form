# Schedule submit form project.
The aim's a tool set time to submit forms by .xlsx file

## Requirement
- Docker
- Setup *.json in src/forms

## Input
- *.xlsx follow setup json file

## How to run
```
docker-compose up
```
OR
```
yarn server;
yarn start;
```
