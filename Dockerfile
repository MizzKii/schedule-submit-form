FROM node:14-alpine3.10 as build
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile

COPY . .
RUN yarn build

FROM node:14-alpine3.10
WORKDIR /app

ENV PORT=80
ENV PUBLIC_PATH=dist

COPY --from=build /app/package.json /app/yarn.lock ./
RUN yarn --frozen-lockfile --production

COPY --from=build /app/build/ ./${PUBLIC_PATH}/
COPY --from=build /app/server/ .

CMD [ "node", "index.js" ]