exports.crosOrigin = (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', '*')
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  next()
}

exports.parseJSON = (req, res, next) => {
  let data = ''
  req.on('data', chunk => data += chunk)
  req.on('end', () => {
    req.raw = data
    try {
      req.json = JSON.parse(`[${data}]`)[0]
    } catch (e) {
      console.error(e)
    }
    next()
  })
}
