const path = require('path')
const express = require('express')
const config = require('./config')
const routers = require('./routers')
const middlewares = require('./middlewares')

const app = express()
// Middlewares
app.use(middlewares.crosOrigin)
app.use(middlewares.parseJSON)

// Routers
app.use('/api', routers.api)

// Serve static file
const publicPath = path.join(__dirname, config.publicPath)
app.use(express.static(publicPath))
app.get('*', (req, res) => res.sendFile(`${publicPath}/index.html`))


const port = process.env.PORT || 80
app.listen(port, () => console.log(`Listen on port ${port}`))
