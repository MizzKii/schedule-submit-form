const axios = require('axios')

const status = {
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  FAILED: 'FAILED',
}

const list = []
exports.list = list
const badRequests = []
exports.badRequests = badRequests
const logResponse = {}
exports.logResponse = logResponse

exports.clean = () => {
  // Set a fake timeout to get the highest timeout id
  const highestTimeoutId = setTimeout(() => {})
  for (let i = highestTimeoutId; i > -1; --i)
    clearTimeout(highestTimeoutId)

  while (list.length) list.pop()
  while (badRequests.length) badRequests.pop()
  for (const key in logResponse) delete logResponse[key]
}

exports.addSchedule = json => {
  const data = {
    retry: 0,
    uri: json.uri,
    time: json.time,
    createTime: Date.now(),
    groupID: json.group_id,
    maxRetry: +json.maximum_retry || 0,
    retryDelay: json.retry_delay || 1000,
    list: json.list.map(item => ({ ...item, status: status.PENDING })),
    payloads: json.list.map(data => {
      const payload = new URLSearchParams()
      Object.keys(data).forEach(key => payload.append(key, data[key]))
      return payload
    })
  }
  list.push(data)
  const duration = json.time - Date.now()
  setTimeout(() => submitGoogleForm(data, 0), duration)
}

const submitGoogleForm = (form, retry) => {
  Promise.all(
    form.payloads.map((payload, index) =>
      new Promise(res => send(form, index, payload, retry).finally(res))
    )
  ).then(() => {
    if (!list.length) return
    if (form.maxRetry <= 0) return
    const fails = form.list.filter(item => item.status !== status.SUCCESS)
    if (fails.length <= 0) return

    const newForm = {...form}
    const time = (getLastOrder(newForm.groupID) || {}).time || Date.now()
    newForm.retry++
    newForm.maxRetry--
    newForm.createTime = Date.now()
    newForm.time = time + newForm.retryDelay
    newForm.list = fails.map(item => ({ ...item, status: status.PENDING }))
    newForm.payloads = newForm.list.map(data => {
      const payload = new URLSearchParams()
      Object.keys(data).forEach(key => key !== 'status' && payload.append(key, data[key]))
      return payload
    })

    list.push(newForm)
    const duration = newForm.time - Date.now()
    setTimeout(() => submitGoogleForm(newForm, retry + 1), duration)
  })
}

const axiosOption = {
  headers: {
    'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
    'Cache-Control': 'no-cache',
    'Pragma': 'no-cache',
    'Expires': '0',
  },
  maxRedirects: 1,
}
const send = (form, index, payload, retry) => {
  const sendTime = new Date()
  const writeLog = writeLogResponse(form.uri)
  const cancelTokenSource = axios.CancelToken.source()

  return axios.post(form.uri, payload, { ...axiosOption, cancelToken: cancelTokenSource.token })
    .then(res => {
      if (res.request.res.responseUrl.endsWith('/closedform')) {
        const err = new Error('closedform')
        err.response = {
          ...res.request.res,
          status: res.request.status || 302,
          statusText: res.request.statusText || 'Temporary redirects',
        }
        throw err
      }
      form.list[index].status = status.SUCCESS
      writeLog(sendTime, retry, res, [...payload.values()])
      cancelTokenSource.cancel()
    })
    .catch(e => {
      form.list[index].status = status.FAILED
      const res = e.response || {}
      if (res.status === 400) badRequests.push(res.data)
      writeLog(sendTime, retry, res, [...payload.values()])
      cancelTokenSource.cancel()
      if (!e.response) console.log(form.groupID, e.message, res.status)
    })
}

const writeLogResponse = uri => (sendTime, retry, res, payloads = []) => {
  const log = {
    'Retry': retry,
    'Send Time': `${sendTime}`,
    'Recive Time': `${(res.headers || {}).date}`,
    'Status code': res.status,
    'Status message': res.statusText,
  }
  for (let i = 0; i < payloads.length; i++) {
    log[`Data [${i+1}]`] = payloads[i]
  }
  if (!logResponse[uri]) logResponse[uri] = []
  logResponse[uri].push(log)
}

const getLastOrder = (groupID) => {
  if (!list.length) return []
  return list.reduce((pre, cur) => {
    if (cur.groupID === groupID && (!pre || cur.time > pre.time)) {
      return cur
    }
    return pre
  })
}
