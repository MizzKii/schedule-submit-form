const express = require('express')
const schedule = require('../models/schedule')

const router = express.Router()

router.get('/schedule', (req, res) => {
  res.json(schedule.list)
})
router.get('/schedule/clean', (req, res) => {
  schedule.clean()
  res.send('Reseted success!')
})
router.post('/schedule', (req, res) => {
  schedule.addSchedule(req.json)
  res.json(req.raw)
})

router.get('/schedule/bad-requests', (req, res) => {
  res.json(schedule.badRequests)
})
router.get('/schedule/bad-requests/:index', (req, res) => {
  const index = req.params.index
  res.send(schedule.badRequests[index] || 'Not found!')
})
router.get('/schedule/logs/:index', (req, res) => {
  const list = Object.values(schedule.logResponse)
  res.json(list[req.params.index] || [])
})

module.exports = router
