import { combineProviders } from './Provider'
import { ScheduleContext, ScheduleProvider } from './schedule'

export const StoreProvider = combineProviders([ScheduleProvider])
export {
  ScheduleContext,
}
