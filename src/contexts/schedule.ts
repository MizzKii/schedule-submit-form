import { Action, ACTIONS } from '../actions'
import { createContextProvider } from './Provider'

export interface ScheduleData {
  [key: string]: string
}
export interface Schedule {
  time: number,
  retry: number,
  groupID?: string
  list: ScheduleData[],
}

const initialState: Schedule[] = []

const reducer = (state: Schedule[], action: Action) => {
  switch (action.type) {
    case ACTIONS.SCHEDULE.INITIAL:
      return action.payload
    case ACTIONS.SCHEDULE.INSERT:
      const schedule: Schedule = {
        retry: 0,
        time: action.payload.time,
        list: action.payload.data,
      }
      return [...state, schedule]
    case ACTIONS.SCHEDULE.DELETE:
      state.splice(action.payload, 1)
      return [...state]
    default: return state
  }
}

const [ScheduleContext, ScheduleProvider] = createContextProvider(reducer, initialState)

export { ScheduleContext, ScheduleProvider }
