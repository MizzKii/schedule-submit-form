import './Layout.scss'
import React from 'react'
import { Container } from 'react-bootstrap'
import Navbar from './Navbar'

interface LayoutProps {
  children: React.ReactNode,
}

const LayoutComponent: React.FC<LayoutProps> = ({ children }) => {
  return (
    <React.Fragment>
      <Navbar />
      <Container>{children}</Container>
    </React.Fragment>
  )
}

export default LayoutComponent
