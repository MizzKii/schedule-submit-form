import { Table } from 'react-bootstrap'

const statusColor = (status: string) => {
  switch (status) {
    case 'PENDING': return { color: 'orange' }
    case 'SUCCESS': return { color: 'green' }
    case 'FAILED': return { color: 'red' }
    default: return {}
  }
}

interface TDProps {
  data: object
}

const TD = ({ data }: TDProps) => {
  const values = Object.values(data)
  return (
    <>
      {values.map(val => <td key={val} style={statusColor(val)}>{val}</td>)}
    </>
  )
}

interface TableDynamicProps {
  rows: Array<any>,
}

const TableDynamic = ({ rows }: TableDynamicProps) => (
  <Table striped bordered hover variant="dark" size="sm"> 
    <tbody>
      {
        rows.map((data, i) =>
          <tr key={i}>
            <TD data={data} />
          </tr>  
        )
      }
    </tbody>
  </Table>
)

export default TableDynamic
