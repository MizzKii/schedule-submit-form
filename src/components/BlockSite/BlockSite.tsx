import moment from 'moment'
import styles from './BlockSite.module.css'

interface BlockSiteProps {
  expiryDate: Date,
}

const BlockSite = ({ expiryDate }: BlockSiteProps) => {
  return (
    <div className={styles.container}>
      <h1>Application expiry in <b>{moment(expiryDate).format('DD MMM yyyy')}</b></h1>
      <h2>Current date: <b>{moment().format('DD MMM yyyy')}</b></h2>
    </div>
  )
}

export default BlockSite
