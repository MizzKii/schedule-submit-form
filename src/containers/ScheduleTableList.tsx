
import moment from 'moment'
import { useContext, useEffect } from 'react'
import { scheduleAction } from '../actions'
import { TableDynamic } from '../components'
import { ScheduleContext } from '../contexts'
import { Schedule } from '../contexts/schedule'

const ScheduleTableList = () => {
  const [schedules, dispatchSchedule] = useContext(ScheduleContext)
  useEffect(() => {
    scheduleAction.init()
      .then(dispatchSchedule)
      .catch(e => alert(e.message))
  }, [dispatchSchedule])
  return (
    <>
      {
        schedules.map((schedule: Schedule, i: number) =>
          <div key={schedule.groupID || `O${i + 1}S${schedule.time}`}>
            <h2>Schedule order: {i + 1}</h2>
            {schedule.groupID && <span>ID: {schedule.groupID}<br /></span>}
            <b>[Retry: {schedule.retry || 0}] Time: {moment(schedule.time).format('DD MMM yyyy THH:mm:ss.SSS')}</b>
            <TableDynamic rows={schedule.list} />
          </div>
        )
      }
      {!schedules.length && <label>Not found schedule list.</label>}
    </>
  )
}

export default ScheduleTableList
