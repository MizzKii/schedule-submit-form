import config from '../config'
import { BlockSite } from '../components'

const BlockSiteContainer = () => {
  const expiry = Date.parse(config.expiryDate)
  const date = new Date()
  date.setDate(date.getDate() -1)
  if (!expiry || expiry > date.getTime()) {
    return <div />
  }
  return <BlockSite expiryDate={new Date(expiry)} />
}

export default BlockSiteContainer
