import DatePicker from 'react-datepicker'
import { Form, Row, Col, Button } from 'react-bootstrap'
import { useState, useContext, ChangeEvent } from 'react'
import forms from '../../forms'
import { scheduleAction } from '../../actions'
import styles from './CreateSchedule.module.css'
import { ScheduleContext } from '../../contexts'
import { IForm, merge } from '../../utilities/form'
import { convertToArray } from '../../utilities/xlsx'

const generateDataList = async (form: IForm, excel: any, startRow: number) => {
  if (!excel) {
    alert('Plese select excel(*.xlsx).')
    return
  }
  const data = await convertToArray(excel, 0)
  const list = merge(form, data).slice(startRow - 1)

  return list
}

interface CreateScheduleProps {
  onSubmit: Function,
}

const CreateSchedule = ({ onSubmit }: CreateScheduleProps) => {
  const dispatchSchedule = useContext(ScheduleContext)[1]
  const [excel, setExcel] = useState(null)
  const [formIndex, setFormIndex] = useState(0)
  const [time, setTime] = useState(new Date())
  const [startRow, setStartRow] = useState(forms[0]['start_row'])
  const [gep, setGep] = useState(1000)
  const [split, setSplit] = useState(0)
  const [isLoop, setLoop] = useState(false)
  const [endTime, setEndTime] = useState(10)
  const [retryCount, setRetryCount] = useState(0)
  const [isSending, setSending] = useState(false)
  const addSchedules = scheduleAction.dispatchSchedules(dispatchSchedule)

  const changeForm = (e: ChangeEvent<HTMLInputElement>) => {
    const index = +e.target.value
    setFormIndex(index)
    setStartRow(forms[index]['start_row'])
  }

  const submit = async () => {
    const list = await generateDataList(forms[formIndex], excel, startRow)
    if (!list) return alert('Not found data!')
    setSending(true)

    try {
      await addSchedules({
        gep,
        list,
        split,
        retryCount,
        time: time.getTime(),
        uri: forms[formIndex].url,
        endTime: isLoop ? time.getTime() + (endTime * 1000) : undefined,
      })
      onSubmit()
    } catch (e) {
      alert(e.message)
    }
    setSending(false)
  }
  return (
    <Form>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Form</Form.Label>
        <Col sm="8">
          <Form.Control as="select" onChange={changeForm}>
            { forms.map((form: any, i: number) => <option key={i} value={i}>{form.name}</option>) }
          </Form.Control>
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Excel</Form.Label>
        <Col sm="8">
          <Form.File accept=".xlsx" onChange={(e: any) => setExcel(e.target.files[0])} />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Time</Form.Label>
        <Col sm="8" className={styles['date-picker']}>
          <DatePicker
            showTimeInput
            selected={time}
            className="form-control"
            onChange={(d: Date) => setTime(d)}
            dateFormat="dd/MM/yyyy HH:mm:ss.SSS"
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Start row</Form.Label>
        <Col sm="8">
          <Form.Control
            type="number"
            value={startRow}
            placeholder="Start row"
            onChange={e => setStartRow(+e.target.value)}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Split</Form.Label>
        <Col sm="2">
          <Form.Control
            type="number"
            value={split}
            placeholder="Split"
            onChange={e => setSplit(+e.target.value)}
          />
        </Col>
        <Form.Label column sm="3">Gap (ms)</Form.Label>
        <Col sm="3">
          <Form.Control
              value={gep}
              type="number"
              placeholder="Gap"
              onChange={e => setGep(+e.target.value)}
            />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Retry count</Form.Label>
        <Col sm="2">
          <Form.Control
            type="number"
            value={retryCount}
            placeholder="Split"
            onChange={e => setRetryCount(+e.target.value)}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column sm="4">Loop</Form.Label>
        <Form.Group controlId="formBasicCheckbox" className={styles['checkbox']}>
          <Form.Check type="checkbox" label={`loop status: ${isLoop ? 'on' : 'off'}`} checked={isLoop} onChange={e => setLoop(e.currentTarget.checked)} />
        </Form.Group>
      </Form.Group>
      {
        isLoop && (
          <Form.Group as={Row}>
            <Col sm={4} />
            <Form.Label column sm="3" className={styles['text-left']}>Limit (Sec)</Form.Label>
            <Col sm="5">
              <Form.Control
                type="number"
                value={endTime}
                placeholder="Seconds"
                onChange={e => setEndTime(+e.target.value > 60 ? 60 : +e.target.value)}
              />
            </Col>
          </Form.Group>
        )
      }
      <Form.Group as={Row}>
        <Col>
          <Button disabled={isSending} variant="primary" onClick={submit}>Save</Button>
        </Col>
      </Form.Group>
    </Form>
  )
}

export default CreateSchedule
