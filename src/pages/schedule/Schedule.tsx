import styles from './Schedule.module.css'
import { ScheduleTableList } from '../../containers'

const SchedulePage = () => (
  <div className={styles['app']}>
    <header className={styles['app-header']}>
      <ScheduleTableList />
    </header>
  </div>
)

export default SchedulePage
