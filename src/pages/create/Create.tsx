import { useState } from 'react'
import { Redirect } from 'react-router-dom'
import styles from './Create.module.css'
import { CreateSchedule } from '../../containers'

const CreatePage = () => {
  const [isRedirect, setRedirect] = useState(false)
  return (
    <div className={styles['app']}>
      <header className={styles['app-header']}>
      {isRedirect && <Redirect to={{ pathname: '/schedule' }} />}
      <CreateSchedule onSubmit={() => setRedirect(true)} />
      </header>
    </div>
  )
}

export default CreatePage
