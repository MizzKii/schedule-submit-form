import { Link } from 'react-router-dom'
import styles from './Home.module.css'
import logo from '../../assets/logo.svg'

const HomePage = () => {
  return (
    <div className={styles['app']}>
      <header className={styles['app-header']}>
        <img src={logo} className={styles['app-logo']} alt="logo" />
        <h2>Wellcome to <code>Google Form</code> project.</h2>
        <p>
          Upload your <code>Excel</code> to setup schedule for submit <code>Google form</code>.
        </p>
        <Link className={styles['app-link']} to="/create">
          Set schedule submit Google form.
        </Link>
      </header>
    </div>
  )
}

export default HomePage
