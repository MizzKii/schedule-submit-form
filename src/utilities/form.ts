import { ExcelArray } from './xlsx'

export interface IForm {
  url: string,
  name: string,
  data: {
    [key: string]: {
      label: string,
      column: string,
      default: string,
    }
  },
}

export const merge = (form: IForm, data: ExcelArray): { [k: string]: string }[] => {
  const keys = Object.keys(form.data)
  return data.map((v, i) => (
    keys.reduce((pre, cur) => {
      const { column, default: def } = form.data[cur]
      return { ...pre, [cur]: v[`${column}${i+1}`] || def }
    }, {})
  ))
}
