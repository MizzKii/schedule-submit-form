import XLSX from 'xlsx'

interface ExcelItem {
  [k: string]: string
}

export interface ExcelArray extends Array<ExcelItem> {
  [i: number]: ExcelItem
}

const read = (file: File): Promise<XLSX.WorkBook> => new Promise((resolve, reject) => {
  const reader = new FileReader()
  // const rABS = !!reader.readAsBinaryString
  reader.onload = (e: any) => {
    const data = /* !rABS? */new Uint8Array(e.target.result) /* : e.target.result */
    const workbook = XLSX.read(data, { type:/* !rABS ?*/ 'array' /*: 'binary'*/ })

    /* DO SOMETHING WITH workbook HERE */
    resolve(workbook)
  }
  // if(!rABS) {
    reader.readAsArrayBuffer(file)
  // } else {
  //   reader.readAsBinaryString(file)
  // }
})

const convertToArray = async (file: File, sheetIndex: number): Promise<ExcelArray> => {
  const wb = await read(file)
  const ws = wb.Sheets[wb.SheetNames[sheetIndex]]
  // const json = XLSX.utils.sheet_to_json(ws)
  const formulae = XLSX.utils.sheet_to_formulae(ws)
  return formulae.reduce((pre: ExcelArray, cur) => {
    const [k, v] = cur.split('=')
    const val = v.startsWith(`'`) ? v.slice(1) : v
    const row = +((k.match(/\d+/g) || [])[0])
    // const col = (k.match(/[a-zA-Z]/g) || [])[0]

    while (pre.length < row) pre.push({})
    pre[row - 1] = { ...pre[row - 1], [k]: val }
    return pre
  }, [])
}

export { read, convertToArray }
