const generateAction = (key: string) => ({
  INSERT: `INSERT_${key}`,
  UPDATE: `UPDATE_${key}`,
  DELETE: `DELETE_${key}`,
})

const ACTIONS = {
  SCHEDULE: { ...generateAction('MESSAGE'), INITIAL: 'INITIAL_MESSAGE'}
}

export default ACTIONS
