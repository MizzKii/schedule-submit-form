import axios from 'axios'
import ACTIONS from './constant'
import { Action } from './interface'
import { ScheduleData } from '../contexts/schedule'

interface ScheduleInput {
  uri: string
  time: number
  list: ScheduleData[]
  split?: number
  gep?: number
  endTime?: number
  retryCount?: number
  groupID?: string
}

export const init = async (): Promise<Action> => {
  const payload = await axios.get('/api/schedule')
  return {
    type: ACTIONS.SCHEDULE.INITIAL,
    payload,
  }
}

export const dispatchSchedules = (dispatch: Function) => async (arg: ScheduleInput) => {
  const { time, split, endTime, retryCount } = arg
  const gep = arg.gep || 0
  let lastTime = time + gep
  const schedules = splitSchedules(arg.list, split || 0)
  let groupID = arg.groupID
  if (retryCount && !groupID) groupID = `T${Date.now()}R${Math.random()}`
  for (let i = 0; i < schedules.length; i++) {
    const list = schedules[i]
    const timeGep = time + (i * gep)
    lastTime = timeGep + gep
    if (endTime && timeGep > endTime) return
    dispatch(await add({ ...arg, list, groupID, gep, time: timeGep }))
  }
  if (endTime && lastTime <= endTime) {
    await new Promise(res => setTimeout(res, 3000))
    dispatchSchedules(dispatch)({ ...arg, groupID, time: lastTime })
  }
}

export const add = async (arg: ScheduleInput): Promise<Action> => {
  const { list, uri, time, groupID, retryCount, gep } = arg
  const res = await axios.post('/api/schedule', {
    uri,
    list,
    time,
    retry_delay: gep,
    group_id: groupID,
    maximum_retry: retryCount,
  })
  const data = JSON.parse(`${res}`).list
  return {
    type: ACTIONS.SCHEDULE.INSERT,
    payload: { time, data },
  }
}

const splitSchedules = (list: ScheduleData[], split: number): Array<ScheduleData[]> => {
  if (split < 1) return [list]
  const res = list.reduce((pre: Array<ScheduleData[]>, cur: ScheduleData) => {
    const index = pre.length && pre.length - 1
    if (!!pre[index] && pre[index].length < split) {
      pre[index] = [...pre[index], cur]
      return pre
    }
    return [...pre, [cur]]
  }, [])
  return res
}

export const remove = (index: number): Action => ({
  type: ACTIONS.SCHEDULE.DELETE,
  payload: index,
})
