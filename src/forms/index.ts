import demo from './demo.json'
import form1 from './form1.json'
import form2 from './form2.json'
import form3 from './form3.json'
import form4 from './form4.json'
import form5 from './form5.json'
import form6 from './form6.json'
import form7 from './form7.json'

const forms = [form1, form2, form3, form4, form5, form6, form7, demo]

export default forms
